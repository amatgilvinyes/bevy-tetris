# Copia barata de Tetris

## Coordenades
Dalt-esquerra és (0,0):
```
(-, -) -- (+, -)
  |         |
  |         |
(-, +) -- (+, +)
```

## Representació interna
- S'emagatzemen els centres de cada peça
- Cada frame, es populen les cel·les al voltant de cada centre



## Tetraminos
Llegenda: `X` és el centre, `O` son altres parts visuals.

L'ordre és: **Cap, Esquerra, Vertical, Dreta**.

- Linia:
```
O            O
X    OXOO    O     OOXO
O            X
O            O
```

- Cub:
```
XO
OO
```

- T
```
        O     O     O
OXO    OX    OXO    XO
 O      O           O
```

- L
```
O     XOO    OX      O
O     O       O    OOX
XO            O
```

- J
```
 O    O       XO   OOX
 O    XOO     O      O
OX            O
```


- Z
TODO

- S
TODO

