use bevy::prelude::*;

use bevy_tetris::declaracions::{Taulell, Tetramino};

fn main() {
    /*
    App::new()
        .add_plugins(DefaultPlugins)

        .run();
        */
    let mut taulell = Taulell::new();

    println!("{taulell}");

    taulell.spawn_peça(Tetramino::Cub);

    println!("{taulell}");

}
