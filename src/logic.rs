use bevy::prelude::*;

use crate::declaracions::{Taulell, TAULELL_ALTURA, TAULELL_AMPLADA, Tetramino, Peça};

impl Taulell {
    pub fn clear_line(&mut self) {
        'Y_LABEL: for y in 0..TAULELL_ALTURA {
            for x in 0..TAULELL_AMPLADA {
                if self.get(x, y).is_none() {continue 'Y_LABEL;}

                if x == TAULELL_AMPLADA - 1 {
                    for nova_x in 0..TAULELL_AMPLADA {
                        *self.get_mut(nova_x, y) = None;
                    }
                }

            }
        }
    }

    pub fn spawn_peça(&mut self, tetramino: Tetramino) {
        let (x, y) = (TAULELL_AMPLADA / 2, 3);

        let peça: Peça = tetramino.into();
        *self.get_mut(x, y) = Some(peça.into());

        self.marcar_quadradets_a_partir_de_peces();

    }
}
