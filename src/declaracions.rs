use std::fmt::Display;

use bevy::utils::HashMap;


pub const TAULELL_ALTURA: usize = 15;
pub const TAULELL_AMPLADA: usize = 8;

pub struct Taulell([Option<Quadrat>; TAULELL_AMPLADA * TAULELL_ALTURA]);

impl Taulell {
    pub fn new() -> Self {
        Taulell([None; TAULELL_ALTURA * TAULELL_AMPLADA])
    }
    pub fn get(&self, x: usize, y: usize) -> &Option<Quadrat> {
        return &self.0[x + TAULELL_AMPLADA*y];
    }

    pub fn get_mut(&mut self, x: usize, y: usize) -> &mut Option<Quadrat> {
        return &mut self.0[x + TAULELL_AMPLADA*y];
    }

    pub fn marcar_quadradets_a_partir_de_peces(&mut self) {
        for y in 0..TAULELL_ALTURA {
        for x in 0..TAULELL_AMPLADA {
            if let Some(quadrat) = self.get(x, y) {
                dbg!(quadrat);
                if let Quadrat::CentrePeça(peça) = quadrat {
                    dbg!(peça);
                    let deltas = peça.posicions_tetraminos();
                    println!("{:#?}", deltas);
                    let tipus_tetramino = peça.tipus;

                    'DELTA_LABEL: for delta in deltas {
                        let new_x = {
                            if x as i32 + delta.0 < 0 {continue 'DELTA_LABEL;}
                            (x as i32 + delta.0) as usize
                        };
                        let new_y = {
                            if y as i32 + delta.1 < 0 {continue 'DELTA_LABEL;}
                            (y as i32 + delta.1) as usize
                        };

                        *self.get_mut(new_x, new_y) = Some(tipus_tetramino.into());
                        println!("set {new_x}, {new_y} to {:?}", tipus_tetramino);
                    }

                }
            }
        }
        }
    }
}

impl Display for Taulell {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let mut output = String::new();
        let char_per_tetra = HashMap::from([
           (Tetramino::Linia, 'L'),
           (Tetramino::Cub, 'C'),
           (Tetramino::L, 'l'),
           (Tetramino::J, 'j'),
           (Tetramino::Z, 'z'),
           (Tetramino::S, 's'),
           (Tetramino::T, 't'),
        ]);

        for _ in 0..TAULELL_AMPLADA {output.push('-');}
        for _ in 0..TAULELL_AMPLADA {output.push('-');}

        for y in 0..TAULELL_ALTURA {
        for x in 0..TAULELL_AMPLADA {
            let c = match *self.get(x, y) {
                None => ' ',
                Some(q) => {
                    let t = match q {
                        Quadrat::CentrePeça(p) => p.tipus,
                        Quadrat::Tetramino(t) => t,
                    };
                    char_per_tetra[&t]
                },
            };
            output.push(c);
        }
        output.push('\n');
        }

        for _ in 0..TAULELL_AMPLADA {output.push('-');}
        for _ in 0..TAULELL_AMPLADA {output.push('-');}
        write!(f, "{}", output)
    }
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Hash)]
pub enum Quadrat {
    CentrePeça(Peça),
    Tetramino(Tetramino),
}

impl From<Tetramino> for Quadrat {
    fn from(value: Tetramino) -> Self {
        Self::Tetramino(value)
    }
}
impl From<Peça> for Quadrat {
    fn from(value: Peça) -> Self {
        Self::CentrePeça(value)
    }
}

#[derive(Debug, Hash, Clone, Copy, PartialEq, PartialOrd, Eq)]
pub enum Tetramino {
    Linia,
    Cub,
    L,
    J,
    Z,
    S,
    T,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Hash)]
pub enum Rotació {
    Cap,
    Esquerra,
    Vertical,
    Dreta,
}

#[derive(Debug, Clone, Copy, PartialEq, PartialOrd, Eq, Hash)]
pub struct Peça {
    tipus: Tetramino,
    rotacio: Rotació,
}

impl Peça {
    fn posicions_tetraminos(&self) -> Vec<(i32, i32)> {
        match self.tipus {
            Tetramino::Cub => Vec::from([(0, 1), (1,1), (1, 0)]),
            Tetramino::Linia => {
                match self.rotacio {
                    Rotació::Cap | Rotació::Vertical => Vec::from([(0, -1), (1, 1), (0, 2)]),
                    Rotació::Esquerra | Rotació::Dreta => Vec::from([(-1, 0), (1, 1), (2, 0)]),
                }
            },
            Tetramino::L => {
                match self.rotacio {
                    Rotació::Cap => Vec::from([(0, 2), (0, 1), (1, 0)]),
                    Rotació::Esquerra => Vec::from([(0, -1), (1, 0), (2, 0)]),
                    Rotació::Vertical => Vec::from([(-1, 0), (0, -1), (0, -2)]),
                    Rotació::Dreta => Vec::from([(0, 1), (-2, 0), (-1, 0)]),
                }
            },
            Tetramino::J => {
                match self.rotacio {
                    Rotació::Cap => Vec::from([(0, 2), (0, 1), (-1, 0)]),
                    Rotació::Esquerra => Vec::from([(0, 1), (1, 0), (2, 0)]),
                    Rotació::Vertical => Vec::from([(1, 0), (0, -1), (0, -2)]),
                    Rotació::Dreta => Vec::from([(0, -1), (-2, 0), (-1, 0)]),
                }
            },
            Tetramino::T => {
                match self.rotacio {
                    Rotació::Cap => Vec::from([(-1, 0), (1, 0), (0, -1)]),
                    Rotació::Esquerra => Vec::from([(0, 1), (0, -1), (-1, 0)]),
                    Rotació::Vertical => Vec::from([(-1, 0), (1, 0), (0, 1)]),
                    Rotació::Dreta => Vec::from([(0, 1), (0, -1), (1, 0)]),
                }
            },
            Tetramino::S => todo!(),
            Tetramino::Z => todo!(),
        }
    }
}

impl From<Tetramino> for Peça {
    fn from(value: Tetramino) -> Self {
        Peça { tipus: value, rotacio: Rotació::Cap }
    }
}

impl Default for Rotació {
    fn default() -> Self {
        Rotació::Cap
    }
}

